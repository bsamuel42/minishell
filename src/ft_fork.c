/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:48:49 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/29 10:43:15 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			ft_child(char *file, t_shell *shell)
{
	int		ret;

	ret = 0;
	ret = execve(file, shell->input, (shell->empty) ? NULL : *(shell->env_tab));
	free(file);
	ft_freetab((void **)shell->input, 0);
	return (ret);
}

int			ft_fork(char *file, t_shell *shell)
{
	pid_t	father;
	int		exec;

	exec = 1;
	father = fork();
	if (father == 0)
		exec = ft_child(file, shell);
	if (father > 0)
	{
		waitpid(father, NULL, 0);
	}
	else if (father < 0)
		exec = 0;
	return (exec);
}
