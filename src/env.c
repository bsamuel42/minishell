/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:03:10 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/29 11:04:21 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env		*ft_newenv(char *name, char *value, t_env *next)
{
	t_env	*new;

	new = (t_env *)malloc(sizeof(t_env) + 1);
	if (!new)
		return (NULL);
	new->value = (char **)malloc(sizeof(char *));
	if (!new->value)
		return (NULL);
	if (name)
	{
		new->name = ft_strdup(name);
		*(new->value) = (value) ? ft_strdup(value) : ft_strdup("");
		new->next = next;
		return (new);
	}
	free(new);
	return (NULL);
}

int			ft_setenv(t_shell *shell, char *name, char *value)
{
	t_env	*tmp;
	t_env	content;

	if (!name || ft_strchr(name, '='))
	{
		ft_putstr_fd("Var name must contain alphanumeric characters.\n", 2);
		return (0);
	}
	ft_unsetenv(shell, name);
	tmp = (shell->env) ? *(shell->env) : NULL;
	content = (t_env){"", NULL, NULL};
	while (tmp && !content.name && tmp->name[0])
		tmp = tmp->next;
	tmp = ft_newenv(name, value, (shell->env) ? *(shell->env) : NULL);
	if (shell->env && tmp)
	{
		*(shell->env) = tmp;
		shell->env_len += 1;
	}
	shell->empty = (tmp) ? 0 : shell->empty;
	ft_envtab(shell);
	return (1);
}

int			ft_unsetenv(t_shell *shell, char *name)
{
	t_env	*tmp;
	t_env	*content;
	t_env	*prev;

	tmp = *(shell->env);
	content = NULL;
	prev = NULL;
	while (name && tmp && ft_strcmp(tmp->name, name))
	{
		prev = tmp;
		tmp = tmp->next;
	}
	content = tmp;
	if (content)
	{
		ft_delenv((prev) ? &prev->next : shell->env, sizeof(content));
		ft_envtab(shell);
	}
	return (1);
}

char		*ft_getenv(t_env **env, char *name)
{
	t_env	*tmp;

	tmp = *env;
	while (tmp)
	{
		if (ft_strcmp(tmp->name, name) == 0)
		{
			return (*(tmp->value));
		}
		else
			tmp = tmp->next;
	}
	return (NULL);
}
