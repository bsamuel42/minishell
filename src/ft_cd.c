/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 10:57:46 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 11:50:06 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char			*pars_path(char *path)
{
	char		*dot;
	int			i;

	i = 0;
	while ((dot = ft_strstr(path, "..")) != NULL)
	{
		if (((dot[2] == '/') || (dot[2] == '\0')) && dot - i > path)
		{
			while ((dot - i) > path && *(dot - i) != '/')
				i++;
			ft_strcpy(dot - i, dot + 2);
		}
	}
	return (path);
}

char			*ft_chdir(char *path, int param)
{
	struct stat	buf;
	int			*er;
	int			ret;

	if (path && param == 0)
	{
		er = (int[3]){access(path, F_OK), stat(path, &buf), access(path, X_OK)};
		if (er[1] == 0 && !(buf.st_mode & S_IFDIR))
		{
			ft_error(path, ": Is not a directory");
			return (NULL);
		}
		else if (er[0] == 0 && er[1] == 0 && (ret = chdir(path)) == 0)
			return (path);
		else
		{
			ft_putstr_fd(path, 2);
			(er[0]) ? ft_putendl_fd(": No such file or directory.", 2) : NULL;
			(er[2] && !er[0]) ? ft_putendl_fd(": Not exec righ", 2) : NULL;
			return (NULL);
		}
	}
	ft_putstr_fd("cd: No home directory.\n", 2);
	return (NULL);
}

int				ft_cd_pars2(t_shell *shell, char *ret, int pwd)
{
	if (ret != NULL)
	{
		ret = malloc(sizeof(char *) * PATH_MAX);
		(ret) ? getcwd(ret, PATH_MAX) : NULL;
		ft_setenv(shell, "OLDPWD", ft_getenv(shell->env, "PWD"));
		ft_setenv(shell, "PWD", ret);
		(pwd) ? ft_putendl(ft_getenv(shell->env, "PWD")) : NULL;
		ft_strdel(&ret);
		return (1);
	}
	return (0);
}

int				ft_cd_pars(t_shell *shell)
{
	char		*ret;
	int			pwd;
	char		*tmp;

	ret = NULL;
	pwd = 0;
	if (shell->input[1] && ft_strcmp(shell->input[1], "-P") == 0)
		ret = (shell->input[2]) ? ft_chdir(shell->input[2], 1) : \
			ft_chdir(ft_getenv(shell->env, "HOME"), 1);
	else if (shell->input[1] && ft_strcmp(shell->input[1], "-L") == 0)
		ret = (shell->input[2]) ? ft_chdir(shell->input[2], 2) : \
			ft_chdir(ft_getenv(shell->env, "HOME"), 2);
	else if (shell->input[1] && ft_strcmp(shell->input[1], "-") == 0)
	{
		tmp = ft_getenv(shell->env, "OLDPWD");
		ret = ft_chdir((tmp) ? tmp : "-", 0);
		pwd = 1;
	}
	else
		ret = (shell->input[1]) ? ft_chdir(shell->input[1], 0) : \
			ft_chdir(ft_getenv(shell->env, "HOME"), 0);
	return (ft_cd_pars2(shell, ret, pwd));
}

int				ft_cd(t_shell *shell)
{
	return (ft_cd_pars(shell));
}
