/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:42:43 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 13:19:19 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			main(int argc, char **argv, char **envp)
{
	char	*input;
	char	*env[4];

	input = NULL;
	if (argc > 1)
	{
		ft_error("minishell", ": dont take arguments");
		return (0);
		if (ft_strcmp(argv[1], "-t") == 0 && argv[2])
			input = argv[2];
	}
	if (*envp == NULL)
	{
		ft_putstr("empty env\n");
		env[0] = "HOME=/";
		env[1] = "PWD=/";
		env[2] = "PATH=/bin";
		env[3] = NULL;
		chdir("/");
		envp = env;
	}
	ft_mshlog(1, NULL);
	ft_minishell(NULL, input, envp);
	return (0);
}
