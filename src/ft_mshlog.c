/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mshlog.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 10:43:33 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/29 10:43:35 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

void	ft_mshlog(int init, char *str)
{
	if (init == 1)
		ft_openlog("/tmp/minishell_log.txt", 1);
	if (str && *str)
		ft_writelog(str);
	if (init == -1)
		ft_closelog();
}
