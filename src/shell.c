/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:38:05 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/22 11:56:24 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_shell			*ft_initshell(void)
{
	t_shell		*shell;

	shell = ft_memalloc(sizeof(t_shell));
	ft_memset((void *)shell, 0, sizeof(t_shell));
	shell->env_tab = ft_memalloc(sizeof(char ***));
	*shell->env_tab = NULL;
	shell->home = NULL;
	if (!shell->env)
		shell->env = ft_memalloc(sizeof(t_env **));
	return (shell);
}

void			ft_destshell(t_shell *shell)
{
	if (*shell->env_tab)
		ft_freetab((void **)*shell->env_tab, 1);
	free(shell->env_tab);
	if (shell->env)
	{
		ft_destenv(shell->env);
		free(shell->env);
	}
	free(shell);
}
