/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_input.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:41:41 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/22 11:42:22 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		parse_input_env(char **envp, t_shell *shell)
{
	int		i;
	char	*tmp[2];
	char	*equ;

	i = -1;
	while (envp && envp[++i])
	{
		equ = 0;
		equ = ft_strchr(envp[i], '=');
		if (equ)
		{
			tmp[0] = ft_strsub(envp[i], 0, equ - envp[i]);
			tmp[1] = ft_strdup(++equ);
			if (tmp[0] && tmp[1])
			{
				ft_setenv(shell, tmp[0], tmp[1]);
				shell->env_len++;
			}
			free(tmp[0]);
			free(tmp[1]);
		}
	}
	shell->path = shell->env ? ft_getenv(shell->env, "PATH") : "";
	shell->home = shell->env ? ft_getenv(shell->env, "HOME") : NULL;
}
