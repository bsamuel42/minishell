/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:40:06 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/29 17:52:26 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		ft_shellexit(t_shell *shell)
{
	ft_putstr("Exiting!!\n");
	if (shell->input)
		ft_freetab((void **)shell->input, 1);
	ft_destshell(shell);
	ft_mshlog(-1, NULL);
	exit(0);
}

int			ft_check_input(t_shell *shell)
{
	int		ret;

	ret = 1;
	if (shell->input && shell->input[0])
	{
		if (ft_strcmp(shell->input[0], "exit") == 0)
			ft_shellexit(shell);
		else
			ret = ft_builtins(shell);
		return (ret);
	}
	return (0);
}

void		ft_prompt(t_shell *shell)
{
	ft_putstr(ft_getenv(shell->env, "PWD"));
	ft_putstr(" > ");
}

void		ft_minishell(t_shell *inshell, char *input, char **envp)
{
	t_shell	*shell;

	if (inshell != NULL)
		shell = inshell;
	else
		shell = ft_initshell();
	if (envp)
		parse_input_env(envp, shell);
	if (input == NULL)
	{
		ft_shlvl(shell);
		while (42)
		{
			ft_prompt(shell);
			if (get_next_line(0, &input) == 0)
				ft_shellexit(shell);
			ft_mshlog(0, input);
			ft_init_exec(input, shell);
		}
	}
	else
		ft_init_exec(input, shell);
	ft_destshell(shell);
}
