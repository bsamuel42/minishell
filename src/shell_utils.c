/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 10:55:44 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/22 10:57:19 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void			lengths(int n, size_t *len, int *weight)
{
	*len = 1;
	if (n >= 0)
	{
		*len = 0;
		n = -n;
	}
	*weight = 1;
	while (n / *weight < -9)
	{
		*weight *= 10;
		*len += 1;
	}
}

char				*ft_itoa_into(int n, char *str)
{
	size_t			len;
	int				weight;
	size_t			cur;

	lengths(n, &len, &weight);
	if (str == NULL)
		return (NULL);
	cur = 0;
	if (n < 0)
	{
		str[cur] = '-';
		cur++;
	}
	else if (n > 0)
		n = -n;
	while (weight >= 1)
	{
		str[cur++] = -(n / weight % 10) + 48;
		weight /= 10;
	}
	str[cur] = '\0';
	return (str);
}

void				ft_shlvl(t_shell *shell)
{
	char			*str;
	char			envstr[101];
	int				lvl;

	str = ft_getenv(shell->env, "SHLVL");
	lvl = (str) ? ft_atoi(str) + 1 : 1;
	ft_itoa_into(lvl, envstr);
	ft_setenv(shell, "SHLVL", envstr);
}
