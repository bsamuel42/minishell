/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 12:03:12 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/22 12:08:58 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		ft_delenv(t_env **link, size_t size)
{
	t_env	*elem;

	if (size)
	{
		elem = *link;
		if (elem->name[0])
			ft_strdel(&elem->name);
		if (*elem->value)
			ft_strdel(elem->value);
		free(elem->value);
		*link = elem->next;
		elem->next = NULL;
		ft_memdel((void **)&elem);
	}
}

int			ft_destenv(t_env **env)
{
	while (*env)
	{
		ft_delenv(env, sizeof(*env));
	}
	return (1);
}

char		**ft_envtab(t_shell *shell)
{
	t_env	*tmp;
	char	**tab;
	int		i;

	if ((tab = malloc(sizeof(char **) * (shell->env_len + 1))) == NULL)
		return (NULL);
	tmp = *(shell->env);
	i = 0;
	while (tmp && tmp->name)
	{
		tab[i++] = ft_strjoinfree(ft_strjoin(tmp->name, "="), \
			ft_strdup((*(tmp->value) ? *(tmp->value) : "")));
		tmp = tmp->next;
	}
	tab[i] = NULL;
	if (shell->env_tab && *shell->env_tab)
		ft_freetab((void **)(*shell->env_tab), 1);
	*shell->env_tab = tab;
	return (tab);
}
