/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtins.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:50:08 by bsamuel           #+#    #+#             */
/*   Updated: 2016/12/05 11:36:16 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_builtins(t_shell *shell)
{
	int		ret;
	char	**input;

	ret = 0;
	input = shell->input;
	if (ft_strcmp(input[0], "cd") == 0)
		ft_cd(shell);
	else if (ft_strcmp(input[0], "env") == 0)
		ft_env(shell, input);
	else if (ft_strcmp(input[0], "setenv") == 0)
		ft_setenv(shell, (input[1]) ? input[1] : NULL, \
			(input[1] && input[2]) ? input[2] : NULL);
	else if (ft_strcmp(input[0], "unsetenv") == 0)
		ft_unsetenv(shell, input[1]);
	else if (ft_strcmp(input[0], "getenv") == 0)
		ft_getenv(shell->env, input[1]);
	else if (ft_strcmp(input[0], "echo") == 0 && input[1])
		ft_puttab_fd(&input[1], 1);
	else if (ft_strcmp(input[0], "clear") == 0)
		ft_putstr("\e[1;1H\e[2J");
	else
		ret = 1;
	return (ret);
}
