/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 12:11:29 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 14:49:49 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			env_pars(t_shell *shell, char **arg)
{
	int		i;
	char	**tab;

	i = 0;
	if (arg[1] && !ft_strcmp(arg[1], "-i"))
	{
		shell->empty = 1;
		i = 1;
	}
	else if (arg[1] && !ft_strcmp(arg[1], "-u") && arg[2])
	{
		ft_unsetenv(shell, arg[2]);
		i = 2;
	}
	else if (arg[1])
	{
		tab = ft_strsplit(arg[1], '=');
		if (tab && tab[1])
			ft_setenv(shell, tab[0], tab[1]);
		else if (tab && tab[0])
			ft_setenv(shell, tab[0], NULL);
		i = 1;
		ft_freetab((void **)tab, 1);
	}
	return (i);
}

int			ft_env(t_shell *shell, char **arg)
{
	t_shell	*tmpshell;
	char	**tab;
	int		i;

	tmpshell = ft_initshell();
	tmpshell->empty = shell->empty;
	if (shell->env_tab && *shell->env_tab && !shell->empty)
		parse_input_env(*shell->env_tab, tmpshell);
	i = env_pars(tmpshell, arg);
	if (arg[i + 1])
	{
		tmpshell->input = &arg[i + 1];
		tmpshell->path = ft_getenv(tmpshell->env, "PATH");
		tmpshell->path = (tmpshell->path) ? tmpshell->path : ft_strdup("");
		if (ft_check_input(tmpshell))
			ft_exec(tmpshell->input[0], tmpshell);
		tmpshell->input = arg;
	}
	else
	{
		tab = (!tmpshell->empty) ? *(tmpshell->env_tab) : NULL;
		ft_puttabendl_fd(tab, 1);
	}
	ft_destshell(tmpshell);
	return (i);
}
