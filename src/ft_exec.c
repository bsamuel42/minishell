/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:43:59 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 12:43:16 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		*ft_check_acces(char *path, char *name)
{
	char	*file;

	file = ft_strjoin(path, name);
	if (access(file, F_OK & X_OK) == 0)
		return (file);
	free(file);
	return (NULL);
}

int			ft_exec(char *cmd, t_shell *shell)
{
	char	**path;
	char	*file;
	char	*name;
	int		i;

	i = -1;
	file = (ft_strchr(cmd, '/')) ? ft_check_acces("", cmd) : NULL;
	if (file == NULL && ft_getenv(shell->env, "PATH"))
	{
		name = ft_strjoin("/", cmd);
		path = ft_strsplit(shell->path, ':');
		while (path[++i] && !(file = ft_check_acces(path[i], name)))
			;
		free(name);
		ft_freetab((void **)path, 1);
	}
	if (file)
	{
		ft_fork(file, shell);
		free(file);
	}
	else
		ft_error(cmd, ": commande not found");
	return ((file != NULL) ? 1 : 0);
}

void		ft_init_exec(char *input, t_shell *shell)
{
	shell->input = (input) ? ft_split(input) : NULL;
	free(input);
	shell->path = ft_getenv(shell->env, "PATH");
	if (ft_check_input(shell))
		ft_exec(shell->input[0], shell);
	if (shell->input)
	{
		ft_freetab((void **)(shell->input), 1);
		shell->input = NULL;
	}
}
