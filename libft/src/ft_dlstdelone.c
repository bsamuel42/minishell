/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstdelone.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 12:00:23 by bsamuel           #+#    #+#             */
/*   Updated: 2016/03/14 09:55:55 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_dlstdelone(t_dlist *list, void (*del)(void *, size_t))
{
	t_edlist	*next;

	next = (list->head)->next;
	del(&((list->head)->content), (list->head)->content_size);
	free(list->head);
	list->head = next;
}
