/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 12:58:30 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 11:47:41 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			word_count(char *str)
{
	int		i;
	int		cnt;

	if (!str || *str == '\0')
		return (0);
	i = 0;
	cnt = 0;
	while (str[i])
	{
		if (str[i] != '\n' && str[i] != '\t' && str[i] != ' ' && \
				str[i] != '\r' && str[i] != '\0')
		{
			if ((str[i + 1] == '\n' || str[i + 1] == '\t' || \
					str[i + 1] == ' ' || str[i + 1] == '\0'))
				cnt++;
		}
		i++;
	}
	return (cnt);
}

char		**ft_split(char *s)
{
	char	**t;
	int		nb_word;
	int		ind;

	ind = 0;
	nb_word = word_count(s);
	t = (nb_word) ? (char **)malloc(sizeof(*t) * (nb_word + 1)) : NULL;
	if (t == NULL)
		return (NULL);
	while (nb_word--)
	{
		while (*s < 33 || *s == 127)
			s++;
		t[ind] = ft_strsub((const char *)s, 0, ft_splitlen(s));
		if (t[ind] == NULL)
			return (NULL);
		s = s + ft_splitlen(s);
		ind++;
	}
	t[ind] = NULL;
	return (t);
}
