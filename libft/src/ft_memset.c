/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 12:16:05 by bsamuel           #+#    #+#             */
/*   Updated: 2015/11/24 16:17:06 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memset(void *b, int c, size_t len)
{
	int				i;
	unsigned char	*a;

	a = (unsigned char*)b;
	i = 0;
	while ((size_t)i < len)
	{
		*a = (unsigned char)c;
		i++;
		a++;
	}
	return (b);
}
