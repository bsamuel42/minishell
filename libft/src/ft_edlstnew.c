/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_edlstnew.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 11:43:37 by bsamuel           #+#    #+#             */
/*   Updated: 2016/03/14 09:52:49 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_edlist		*ft_edlstnew(void const *content, size_t content_size)
{
	t_edlist	*new;

	if ((new = (t_edlist *)malloc(sizeof(t_edlist *))) == NULL)
		return (NULL);
	new->content = (void *)content;
	new->content_size = content_size;
	new->next = NULL;
	new->prev = NULL;
	return (new);
}
