/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrimsplit.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 12:59:55 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/22 12:59:58 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_strtrimsplit(char *str, char c)
{
	char	**tab;
	char	*tmp;
	int		i;

	i = -1;
	tab = ft_strsplit(str, c);
	while (tab[++i])
	{
		tmp = ft_strtrim(tab[i]);
		ft_strdel(&tab[i]);
		tab[i] = tmp;
	}
	return (tab);
}
