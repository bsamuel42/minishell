/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoinfree.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:59:17 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/07 12:59:20 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoinfree(char const *s1, char const *s2)
{
	char	*tmp;

	tmp = ft_strnew(ft_strlen(s1) + ft_strlen(s2));
	tmp = ft_strcpy(tmp, s1);
	tmp = ft_strcat(tmp, s2);
	ft_strdel((char **)&s1);
	ft_strdel((char **)&s2);
	return (tmp);
}
