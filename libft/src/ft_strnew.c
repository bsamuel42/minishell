/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:06:10 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/07 13:17:21 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char		*ft_strnew(size_t size)
{
	char	*new;
	int		i;

	if ((int)size < 0)
		return (NULL);
	new = malloc(sizeof(char) * ((int)size + 1));
	i = 0;
	while (new && (i < (int)size + 1))
	{
		new[i] = '\0';
		i++;
	}
	return (new);
}
