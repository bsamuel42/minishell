/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsubfree.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:47:07 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/07 12:48:40 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char		*ft_strsubfree(char const *s, unsigned int start, size_t len)
{
	char	*new;
	int		i;

	new = ft_strnew(len);
	if (!new || (start + len > ft_strlen(s)))
		return (NULL);
	i = 0;
	while (i < (int)len)
	{
		new[i] = s[start + i];
		i++;
	}
	new[i] = '\0';
	ft_strdel((char **)&s);
	return (new);
}
