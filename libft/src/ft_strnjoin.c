/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tifuzeau <tifuzeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/09 19:51:32 by tifuzeau          #+#    #+#             */
/*   Updated: 2016/02/19 10:44:20 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnjoin(char const *s1, char const *s2, size_t len_s2)
{
	char	*ptr;
	int		i;
	int		len;

	ptr = NULL;
	if (!s1)
		s1 = "";
	len = ft_strlen(s1);
	if (s2
		&& (ptr = ft_strnew(len + len_s2)))
	{
		ft_strcpy(ptr, s1);
		i = (len == 0) ? 0 : len - 1;
		ft_strncat(&ptr[i], s2, len_s2);
	}
	return (ptr);
}
