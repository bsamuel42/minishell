/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 10:25:36 by bsamuel           #+#    #+#             */
/*   Updated: 2015/11/28 12:11:58 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*link;
	t_list	*tmp;

	new = NULL;
	link = NULL;
	tmp = lst;
	while (tmp != NULL)
	{
		if (new == NULL)
		{
			new = f(tmp);
			link = new;
		}
		else
		{
			link->next = f(tmp);
		}
		tmp = tmp->next;
	}
	return (new);
}
