/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 13:33:30 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/07 13:22:17 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void				*cpy(unsigned char *dst, unsigned char *src, size_t n)
{
	int				i;

	i = 0;
	while ((size_t)i < n)
	{
		*dst = *src;
		i++;
		dst++;
		src++;
	}
	return (dst);
}

int					overlap_p(void *a, void *b, size_t n)
{
	unsigned int	i;

	i = 0;
	while (++i < n)
	{
		if (a + i == b || b + i == a)
			return (1);
		i++;
	}
	return (0);
}

void				*ft_memcpy(void *dst, void *src, size_t n)
{
	cpy((unsigned char *)dst, (unsigned char *)src, n);
	return (dst);
}
