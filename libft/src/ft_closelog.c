/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_closelog.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 11:21:53 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/29 11:24:43 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

int			ft_closelog(void)
{
	int		fd;

	fd = ft_openlog(NULL, 0);
	if (fd > 2)
		if (close(fd) == -1)
			return (0);
	return (1);
}
