/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 11:15:28 by bsamuel           #+#    #+#             */
/*   Updated: 2016/03/14 09:51:22 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"
#include <stdlib.h>

char	*ft_strndup(const char *s1, size_t n)
{
	char *s2;

	if ((s2 = ft_strnew(n)) == NULL)
		return (NULL);
	ft_strncpy(s2, s1, n);
	return (s2);
}
