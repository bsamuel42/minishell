/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 14:32:44 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/07 13:22:48 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

void				*ccpy(unsigned char *dst, unsigned char *src, \
		unsigned char c, size_t n)
{
	unsigned int	i;

	i = 0;
	while (i < n)
	{
		if ((*dst++ = *src++) == c)
			return (dst);
		i++;
	}
	return (NULL);
}

void				*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	return (ccpy((unsigned char *)dst, (unsigned char *)src, \
				(unsigned char)c, n));
}
