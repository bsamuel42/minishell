/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 11:43:37 by bsamuel           #+#    #+#             */
/*   Updated: 2016/03/14 09:53:32 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_dlist	*ft_dlstnew(void const *content, size_t content_size)
{
	t_dlist	*new;

	if ((new = (t_dlist *)malloc(sizeof(t_dlist *))) == NULL)
		return (NULL);
	new->nb = 0;
	new->head = NULL;
	new->tail = NULL;
	return (new);
}
