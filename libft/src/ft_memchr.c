/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:26:49 by bsamuel           #+#    #+#             */
/*   Updated: 2015/11/26 16:24:05 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned const char	*tmp_s;

	tmp_s = s;
	while (n-- != 0)
	{
		if (*tmp_s == (unsigned char)c)
		{
			return ((void *)tmp_s);
		}
		else
		{
			++tmp_s;
		}
	}
	return (NULL);
}
