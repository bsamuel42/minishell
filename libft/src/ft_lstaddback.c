/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstaddback.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 13:23:22 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/07 13:23:26 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstaddback(t_list **blst, void const *content,
		size_t content_size)
{
	t_list	*list;

	list = *blst;
	if (list)
	{
		while (list->next)
			list = list->next;
		list->next = ft_lstnew(content, content_size);
	}
	else
		*blst = ft_lstnew(content, content_size);
}

void	ft_lstnewaddback(t_list **blst, t_list *new)
{
	t_list	*list;

	list = *blst;
	new->next = NULL;
	if (list)
	{
		while (list->next)
			list = list->next;
		list->next = new;
	}
	else
		*blst = new;
}
