/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_openlog.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 11:21:49 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 15:00:54 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <fcntl.h>
#include "libft.h"

int				ft_openlog(char *file, int init)
{
	static	int	fd;

	if (init)
		fd = open(file, O_CREAT | O_RDWR | O_APPEND, 666);
	return (fd);
}
