/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstadd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 11:48:02 by bsamuel           #+#    #+#             */
/*   Updated: 2015/11/30 11:59:16 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_dlstadd(t_dlist *list, t_edlist *link)
{
	t_dlist		*debut;

	debut = list->head;
	if (debut != NULL)
	{
		link->next = debut;
		debut->prev = link;
	}
	list->head = link;
	if (list->tail == NULL)
		list->tail = link;
}

void			ft_dlsttailadd(t_dlist *list, t_edlist *link)
{
	t_dlist		*debut;

	debut = list->tail;
	if (debut != NULL)
	{
		link->prev = debut;
		debut->next = link;
	}
	list->tail = link;
	if (list->head == NULL)
		list->head = link;
}
