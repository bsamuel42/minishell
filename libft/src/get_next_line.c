/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 13:39:54 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/22 12:57:48 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

static int	get_line(t_line *tline, char **line)
{
	char	*next;

	if (!tline->str || !ft_strlen(tline->str))
		return (0);
	if (!tline->newline)
	{
		*line = tline->str;
		tline->str = ft_strnew(0);
		return (1);
	}
	next = ft_strchr(tline->str, '\n');
	*next = '\0';
	*line = ft_strdup(tline->str);
	ft_strcpy(tline->str, next + 1);
	if (!next || *(next) == '\0')
		ft_strdel(&tline->str);
	tline->newline--;
	return (1);
}

int			get_next_line(const int fd, char **line)
{
	char				*buff;
	char				*tmp;
	int					ret;
	static t_line		tline;

	while (!tline.newline)
	{
		buff = ft_strnew(BUFF_SIZE);
		ret = read(fd, buff, BUFF_SIZE);
		if (ret > 0)
		{
			tline.newline = ft_countchar(buff, '\n');
			tmp = (tline.str) ? ft_strjoin(tline.str, buff) : ft_strdup(buff);
			free(tline.str);
			tline.str = tmp;
		}
		free(buff);
		if (ret == -1)
			return (-1);
		if (ret == 0)
			return (get_line(&tline, line));
	}
	return (get_line(&tline, line));
}
