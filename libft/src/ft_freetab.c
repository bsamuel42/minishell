/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freetab.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 13:16:08 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/17 12:21:30 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_freetab(void **tab, int origine)
{
	int	i;

	i = -1;
	if (tab)
	{
		while (tab[++i])
			ft_memdel(&tab[i]);
		*tab = NULL;
		if (origine)
			ft_memdel((void **)&tab);
	}
}
