/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstdel.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 12:01:21 by bsamuel           #+#    #+#             */
/*   Updated: 2016/03/14 09:55:37 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_dlstdel(t_dlist *list, void (*del)(void *, size_t))
{
	while (list->head != NULL)
	{
		list->tail = (list->head == list->tail) ? NULL : list->tail;
		ft_dlstdelone(list, del);
	}
}
