/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wlen.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 11:10:41 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/22 13:12:04 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"

int		ft_wlen(const char *s, char c)
{
	int		len;

	len = 0;
	while (*s != c && *s != '\0')
	{
		len++;
		s++;
	}
	if (*s != c && *s != '\0')
		len = -len;
	return (len);
}

int		ft_splitlen(const char *s)
{
	int		len;

	len = 0;
	while (*s != '\0' && *s != '\t' && *s != '\n' && *s != ' ')
	{
		len++;
		s++;
	}
	if (*s != '\0' && *s != '\t' && *s != '\n' && *s != ' ')
		len = -len;
	return (len);
}
