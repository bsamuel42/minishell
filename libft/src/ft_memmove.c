/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:27:27 by bsamuel           #+#    #+#             */
/*   Updated: 2015/11/26 10:28:15 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*c1;
	const char	*c2;

	c2 = src;
	c1 = dst;
	if (dst <= src)
		return (ft_memcpy(dst, (void *)src, len));
	c2 += len;
	c1 += len;
	while (len--)
		*--c1 = *--c2;
	return (dst);
}
