/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puttab_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 10:48:32 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 14:53:45 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_puttab_fd(char **s, int fd)
{
	int		i;

	i = 0;
	while (s && s[i] != NULL)
	{
		ft_putstr_fd(s[i], fd);
		ft_putchar_fd(' ', fd);
		i++;
	}
	ft_putchar_fd('\n', fd);
}

void	ft_puttabendl_fd(char **s, int fd)
{
	int		i;

	i = 0;
	while (s && s[i] != NULL)
	{
		ft_putendl_fd(s[i], fd);
		i++;
	}
}
