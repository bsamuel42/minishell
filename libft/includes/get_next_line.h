/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 08:15:59 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/17 11:06:44 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

typedef struct	s_line
{
	char		*str;
	int			newline;
}				t_line;

int				get_next_line(const int fd, char **line);

# define BUFF_SIZE 255

#endif
