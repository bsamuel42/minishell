NAME	= minishell 

INC_PATH= ./includes/
OBJ_PATH= ./obj/
SRC_PATH= ./src/

SRC_NAME		= main.c minishell.c shell_utils.c env.c env_utils.c \
				ft_builtins.c ft_cd.c ft_fork.c ft_exec.c parse_input.c \
				shell.c ft_env.c ft_mshlog.c ft_error.c

OBJ_NAME		= $(SRC_NAME:.c=.o)

INC		= $(addprefix -I,$(INC_PATH))
OBJ		= $(addprefix $(OBJ_PATH),$(OBJ_NAME))
SRC		= $(addprefix $(SRC_PATH),$(SRC_NAME))

CC		= gcc
CC_FLAG	= -Wall -Werror -Wextra -g

all: $(NAME)

$(NAME): $(OBJ_PATH) $(OBJ)
	@make -C libft/
	@printf "Creation de $(NAME):"
	@$(CC) -g $(OBJ) -o $(NAME) -L libft/ -lft
	@printf " \033[0;33mOK\033[0;0m\n"

$(OBJ_PATH):
	@printf "Creation de $(OBJ_PATH):"
	@mkdir -p $(OBJ_PATH) 2> /dev/null
	@printf " \033[0;33mOK\033[0;0m\n"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@printf "Creation de $@:"
	@$(CC) $(CC_FLAG) $(INC) -o $@ -c $<
	@printf " done\n"

clean:
	@make -C libft/ fclean
	@printf "Clean des obj:"
	@$(RM) -f $(OBJ)
	@printf " done\n"
	@printf "Clean $(OBJ_PATH):"
	@$(RM) -rf $(OBJ_PATH) 2> /dev/null
	@printf " done\n"

fclean: clean
	@make -C libft/ fclean
	@printf "Clean $(NAME):"
	@$(RM) -f $(NAME)
	@printf " done\n"

re: fclean all

norme:
	@norminette $(SRC)
	@norminette $(INC_PATH)
