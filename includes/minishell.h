/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsamuel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 13:17:44 by bsamuel           #+#    #+#             */
/*   Updated: 2016/11/30 11:41:23 by bsamuel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include <unistd.h>
# include <stdio.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include "libft.h"
# ifdef __APPLE__
#  include <sys/syslimits.h>
# else
#  include <linux/limits.h>
# endif

typedef struct		s_env
{
	char			*name;
	char			**value;
	struct s_env	*next;
}					t_env;

typedef struct		s_shell
{
	char			**input;
	t_env			**env;
	int				env_len;
	char			***env_tab;
	char			*path;
	char			*home;
	int				empty;
}					t_shell;

t_shell				*ft_initshell();
void				ft_destshell(t_shell *shell);

int					ft_setenv(t_shell *shell, char *name, char *value);
int					ft_unsetenv(t_shell *shell, char *name);
char				**ft_envtab(t_shell *shell);
void				ft_shlvl(t_shell *shell);
t_env				*ft_newenv(char *name, char *value, t_env *next);
char				*ft_getenv(t_env **env, char *name);
int					ft_env(t_shell *shell, char **arg);
void				ft_delenv(t_env **link, size_t size);
int					ft_destenv(t_env **env);

int					ft_check_input(t_shell *shell);
void				ft_minishell(t_shell *shell, char *input, char **envp);
int					ft_fork(char *file, t_shell *shell);
void				ft_init_exec(char *input, t_shell *shell);
int					ft_exec(char *cmd, t_shell *shell);
int					ft_builtins(t_shell *shell);
int					ft_cd(t_shell *shell);
char				*ft_chdir(char *path, int param);

void				parse_input_env(char **envp, t_shell *shell);
void				ft_mshlog(int init, char *str);
void				ft_error(char *name, char *errmsg);
#endif
